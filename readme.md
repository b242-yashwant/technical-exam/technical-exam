## Links to quizzes
Concepts and Theory - https://docs.google.com/forms/d/e/1FAIpQLSfmtiSdlpRN5fK7rKaI-a31PqYaCTKvpnJ_892jsWUEzs4Sqg/closedform

Error Debugging and Code Tracing - https://docs.google.com/forms/d/e/1FAIpQLSdrtrUJJ_nmLJist-XRvTKihYf2g3ek6CTK_yGX7TSuDudgQQ/closedform


## GENERAL REMINDER FOR FUNCTION CODING:
**DO NOT USE ANY ARRAY METHODS AS  THIS DEFEATS THE PURPOSE CREATING FUNCTIONS AND ALGORITHMS IN THE MOCH TECHNICAL EXAM**

Clone this repo to perform the mock tech exam the instructor will give a range of time for  you to complete it to simulate a real-world mock tech exam of possible employers

## Instructions for Mock Technical Exam (Data Structures and Algorithm):
1) use npm install before refactoring any codes
2) Try npm test command
3) Create your function coding in index.js
4) DO NOT refactor the test.js
5) DO npm test every time there is a change in the code that needed to be test
6) Take a screenshot of your terminal when you’re done, attached an image in the mock tech folder.

## Instructions for Mock Technical Exam (Function Coding):
1) Create your **function coding in index.js**
2) **DO NOT** refactor the test.js
3) Create functions for the following functionalities:
	- Print (output all the elements of the queue)
	- Enqueue (add element to rear of queue)
	- Dequeue (remove element at front of queue)
	- Front (show element at the front)
	- Size (show total number of elements)
	- isEmpty (outputs Boolean value describing whether queue is empty or not)


## Instructions for gitlab pushing
1) Create git repository named **Mock Technical Exam**
2) Add remote link and push to git with the commit message of **Add Activity Code**
3) Add the link in boodle and notify the instructor after doing so.